﻿using MMK.Core;
using MMK.Match3;
using UnityEngine;

public class Match3LevelProviderComponent : MonoBehaviour
{
    public string levelsResourcesFolder;

    private void Start ()
    {
        var levelProvider = new Match3LevelProviderResources(this, levelsResourcesFolder);
        LevelProvider<Match3PremadeLevel>.SetInstance(levelProvider);
    }
}
